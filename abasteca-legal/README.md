# app-abasteca-legal - Alpha Cod3rs

ORIENTAÇÕES PARA UTILIZAR O GIT LAB ~~

~PARA PEGAR O PROJETO DO REPOSITÓRIO~

1º Abra o terminal ou cmd, vá para a pasta raiz do projeto(abasteca-legal) e digite o seguinte comando:

        git pull
Logo em seguida será pedido seu login e senha. Preencha-os e o projeto será atualizado na página.

~PARA ENVIAR O PROJETO PARA O REPOSITORIO~

1º Abra o terminal ou cmd, vá para a pasta raiz do projeto(abasteca-legal) e digite o seguinte comando:

    git add .

2º Depois será necessário dar um commit, ou seja, um comentário sobre a modificação realizada no projeto, com o seguinte comando:

    git commit -m "Escreva aqui seu comentário"

3º Depois deste passo, você precisará adicionar o projeto no gitlab com o comando:

    git push origin master

4º Será pedido o usuário e a senha do gitlab. Preencha-os e pronto.
