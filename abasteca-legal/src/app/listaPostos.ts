import { Posto } from './posto';

export const POSTOS: Posto[] = [
    {
        CNPJ: '05.138.015/0001-34', 
        nome: 'Petrobrás - Eixo W 405 sul',
        bandeira: 'Petrobrás',
        preco_gasolina: 4.44,
        preco_etanol: 3.19,
        preco_diesel: 3.09,
        endereco: 'SQS, Bloco A,PLL, 405 - Asa Sul - Brasília, DF, 70239-010',
        responsavel: 'Felipe Smith',
        tel_01: '61 985621765',
        tel_02: '61 34293569',
        email: 'petrobras405sul@gmail.com'
    }
];