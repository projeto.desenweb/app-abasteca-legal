import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PerfilPostoComponent } from './perfil-posto/perfil-posto.component';
import { PageCatalogoComponent } from './page-catalogo/page-catalogo.component';
import { PagePrincipalComponent } from './page-principal/page-principal.component';


@NgModule({
  declarations: [
    AppComponent,
    PerfilPostoComponent,
    PageCatalogoComponent,
    PagePrincipalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
