import { Component, OnInit } from '@angular/core';
import { POSTOS } from '../listaPostos';

@Component({
  selector: 'app-perfil-posto',
  templateUrl: './perfil-posto.component.html',
  styleUrls: ['./perfil-posto.component.css']
})
export class PerfilPostoComponent implements OnInit {

  postos = POSTOS;

  constructor() { }

  ngOnInit() {
  }

}
